const express = require("./node_modules/express");
const router = express.Router();

const User = require("../models/User");

//@route POST api/users

router.post("/check", async (req, res) => {
  const { name, email } = req.body;

  try {
    const user = await User.findOne({ email });

    if (user && !user.hasWinned) {
      return res.json("Puedes participar");
    } else if (user) {
      return res.status(400).json({ errors: [{ msg: "Ud ya participo" }] });
    }

    newUser = new User({
      name,
      email
    });

    newUser.save(err => {
      if (err) throw err;

      return res.json("Puedes participar");
    });
  } catch (err) {
    console.error(err.message);
    return res.status(500).send("Server error");
  }
});

router.post("/save-reward", async (req, res) => {
  const { name, email, reward } = req.body;

  try {
    const user = await User.findOneAndUpdate(
      { email },
      { $set: { hasWinned: true, reward } }
    )
      .where("hasWinned")
      .equals(false);

    if (!user) {
      return res.status(400).json({ errors: [{ msg: "Ud ya participo" }] });
    } else {
      return res.json("Felicidades");
    }
  } catch (err) {
    console.error(err.message);
    return res.status(500).send("Server error");
  }
});

module.exports = router;
