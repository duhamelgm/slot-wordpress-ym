import React from "react";

import "./App.css";
import Contenido from "../src/components/contenido";
import Machine from "./components/Machine";
import Top from "./components/top"
import MidContent from "../src/components/midContent"


function App() {
  return (
   
  <div className="body">
    <div className="styleheader"> <Top/></div>
    <div className="stylemidContent"> <MidContent/> </div>
    <div className="stylecontenido"> <Contenido/></div>
    <div className="stylemachine"> <Machine/></div>

       
  </div>
  );
}

export default App;
