import React, { Component } from "react";

class FacebookLogin extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.initFacebookSDK();
  }

  initFacebookSDK = () => {
    (function(d, s, id) {
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");

    // Init credentials
    window.fbAsyncInit = function() {
      window.FB.init({
        appId: 313867169543029,
        cookie: true, // enable cookies to allow the server to access
        // the session
        xfbml: true, // parse social plugins on this page
        version: "v3.3" // The Graph API version to use for the call
      });

      window.FB.AppEvents.logPageView();
    };

    window.checkLoginState = () => {
      window.FB.getLoginStatus(response => {
        if (!response.status === "connected") return;

        window.FB.api(
          "/me",
          { fields: "first_name, last_name, email" },
          userData => {
            const user = {
              email: userData.email,
              name: userData.first_name
            };

            this.props.checkUser(user);
          }
        );
      }, true);
    };
  };

  /*
  window.fbAsyncInit = () => {
    window.FB.init({
      appId      : '313867169543029',
      xfbml      : true,
      version    : 'v3.3'
    });
   
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));*/

  render() {
    return (
      <div className="botonfb">
        
        <div
          className="fb-login-button"
          data-size="medium"
          data-button-type="login_with"
          data-auto-logout-link="false"
          data-use-continue-as="true"
          data-onlogin="checkLoginState();"
          data-scope="public_profile,email"
        />
      </div>
    );
  }
}

export default FacebookLogin;
