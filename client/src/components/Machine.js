import React, { Component } from "react";
import { TimelineLite, Power4 } from "gsap/TweenMax";
import "../assets/css/Machine.css";

class Machine extends Component {
  constructor(props) {
    super(props);
    this.slot1 = null;
    this.slot2 = null;
    this.slot3 = null;
    this.spin = null;
  }

  componentDidMount() {
    this.spin = [
      new Audio("/sounds/spin.mp3"),
      new Audio("/sounds/spin.mp3"),
      new Audio("/sounds/spin.mp3"),
      new Audio("/sounds/spin.mp3"),
      new Audio("/sounds/spin.mp3"),
      new Audio("/sounds/spin.mp3"),
      new Audio("/sounds/spin.mp3")
    ];
  }

  runSlot = () => {
    const sounds = {
      spin: this.spin
    };
    let soundPlaying = 0;
    const tl = new TimelineLite({
      onComplete: this.slotFinish
    });

    var numChanges = randomInt(1, 4) * 6;
    //var numeberSlot1 = numChanges + 20;
    // var numeberSlot2 = numChanges + 2 * 6 + randomInt(1, 6);
    //var numeberSlot3 = numChanges + 20;

    tl.staggerTo(
      [this.slot1, this.slot2, this.slot3],
      0.03,
      {
        ease: Power4.easeOut,
        repeat: -1,
        onStart: function() {
          const id = this.target.id;
          if (id === "slot1") {
            this.numberSlot = numChanges + 20;
          } else if (id === "slot2") {
            this.numberSlot = numChanges + 2 * 6 + randomInt(1, 6);
          } else if (id === "slot3") {
            this.numberSlot = numChanges + 32;
          }

          this.iterations = 0;
        },
        onRepeat: function() {
          const nextTarget = parseInt(this.target.className.substring(1)) + 1;
          this.target.className = "a" + (nextTarget > 6 ? 1 : nextTarget);

          sounds.spin[soundPlaying++].play();
          soundPlaying = soundPlaying > 6 ? 0 : soundPlaying;

          this.iterations++;
          if (this.iterations > this.numberSlot) this.kill();
        }
      },
      0.25
    );
  };

  slotFinish = () => {};

  render() {
    return (
      <main>
        <section id="Slots" classname="cajaslots">
          <div id="slot1" ref={el => (this.slot1 = el)} class="a6" />
          <div id="slot2" ref={el => (this.slot2 = el)} class="a6" />
          <div id="slot3" ref={el => (this.slot3 = el)} class="a6" />
        </section>
        <section onClick={this.runSlot} id="Gira" />
      </main>
    );
  }
}


function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export default Machine;
