import React from "react";
import "../assets/css/midcontent.css"


class MidContent extends React.Component {
    render(){
        return(
            <div className="contenido">
                <h1 className="encabezado">¡FELIZ CUMPLE PARA NOSOTROS!</h1>

                <h2 className="encabezadoSecundario"> CUMPLIMOS 1 AÑO Y LO FESTEJAMOS CON VOS.</h2>
            </div>

        );
    }
}
export default MidContent