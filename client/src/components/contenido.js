import React, { Component } from "react";
import axios from "axios";
import FacebookLogin from "./facebookLogin";
import plus from "../assets/imgs/plus.png";
import SecondInput from "./imputSecond"
import "../assets/css/contenido.css";

class Contenido extends Component {
  checkUser = async user => {
    console.log(user);
    const response = await axios.post("/api/user/check", user);

    console.log(response);
  };

  render() {
    return (
        <div>
        <div className="barraLateral">
          <h1 className="participa">PARTICIPÁ Y GANÁ</h1>

          <h3 className="info">
            {" "}
            <img src={plus} className="plus1" alt="plus" />
            Productos
          </h3>
          <h3 className="info">
            {" "}
            <img src={plus} className="plus1" alt="plus" />
            Descuentos
          </h3>
          <h3 className="info">
            {" "}
            <img src={plus} className="plus1" alt="plus" />
            Envíos sin cargo
          </h3>
          <p className="mail"> ¡Ingresá tu mail o logueate con Facebook y juegá!</p>
          </div>
        <div />
       
        <div className="botonfbmain">
          <SecondInput className="second"/>
          <FacebookLogin classname= "botonfbk" checkUser={this.checkUser} />
        </div>
        <div className="relleno">
          <h4 className="relleno2">
          Mucha suerte y gracias por acompañarnos durante
          este primer año. 
          </h4>
          <h1 className="relleno3"> ¡POR MUCHOS MÁS! </h1>

        </div>
      </div>
    );
  }
}

export default Contenido;
