const path = require("path");
const express = require("express");
const connectDB = require("./config/db");
const userRoutes = require("./Api/users");

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Connect DataBase
connectDB();

app.use("/api/user", userRoutes);

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "/client/build/index.html"));
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log("Server started on 5000"));
